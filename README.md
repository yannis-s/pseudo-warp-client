## A simple set of scripts and config entries for cloudflare-warp

This is an unofficial Warp "client".
This was made because  **warp-taskbar** doesn't play along with **i3** and **polybar** and I didn't want to troubleshoot.

#### Prerequisites
 - i3
 - polybar
 - [cloudflare-warp](https://pkg.cloudflareclient.com/)
 - [Enroll](https://developers.cloudflare.com/cloudflare-one/connections/connect-devices/warp/deployment/manual-deployment/#enroll-via-the-cli) to Cloudflare before using the scripts
 - [Font Awesome 5](https://www.youtube.com/watch?v=ws8cu1dWJOo) (not my video, but is an easy guide how to install fonts)
 - Optional a secondary terminal emulator that can render Font awesome 5 it will be always running in floating mode (lxterminal did the trick for me)

---

The config files contain all the lines to add in your *i3/config* and *polybar/config*, and the comments explain what they do.

---

*Screenshots:*

![Screenshot 1](images/Screenshot_1.png)
![Screenshot 2](images/Screenshot_2.png)
