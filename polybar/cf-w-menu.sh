#!/bin/bash


# Color scheme
WHITE="\e[01;37m"
GREEN="\e[01;32m"
YELLOW="\e[01;33m"
RED="\e[01;31m"
CYAN="\e[01;36m"
BORANGE="\e[033;48;5;202m "
ORANGE="\e[38;5;202m"
RESET="\e[00m"
# Feel free to change colors to your liking

wenroll () {
    read -p "Enter your organization team:  " team
    warp-cli registration new ${team}
}

wstatus () {
    tsa=$(warp-cli status | grep -i "disconnected" | awk '{print $3}' | grep -i "disconnected")

    if [[ $tsa =~ "Disconnected" ]]; then
        echo -e "${RED}   Disconected"
    else
        echo -e "${GREEN}   Connected"
    fi
}

pressanykey () {
    echo
    read -n 1 -s -r -p "Press any key to continue"
}

menu () {
while true; do
    clear
    echo -e "${BORANGE}                                                           ${RESET}"
    echo -e "${BORANGE}${YELLOW}                   CF Zero Trust - Warp                    ${RESET}"
    echo -e "${BORANGE}                                                           ${RESET}"
    echo -e "${CYAN} 1 ${GREEN}$(printf "\u2192\n") ${WHITE}Connect"
    echo -e "${CYAN} 2 ${GREEN}$(printf "\u2192\n") ${WHITE}Disconect"
    echo -e "${CYAN} 3 ${GREEN}$(printf "\u2192\n") ${WHITE}WARP status"
    echo -e "${CYAN} 4 ${GREEN}$(printf "\u2192\n") ${WHITE}Reauthenticate user session"
    echo -e "${CYAN} 5 ${GREEN}$(printf "\u2192\n") ${WHITE}Enroll with your organization team"
    echo -e "${CYAN} 6 ${GREEN}$(printf "\u2192\n") ${WHITE}Delete old registration"
    echo -e "${RED} q ${GREEN}$(printf "\u2192\n") ${RED}Quit${RESET}"
    echo -e "${BORF}                                                           ${RESET}"
    echo -e "${BORANGE}                                                           ${RESET}"
    echo -e "${BORANGE}${YELLOW}            Visit: https://gitlab.com/yannis-s             ${RESET}"
    echo -e "${BORANGE}                                                           ${RESET}${YELLOW}\n"
    read -p "Enter your choice: " menuChoice
    case ${menuChoice} in

        1)
            echo
            echo -e "${ORANGE}"
            warp-cli connect && wstatus
            echo -e "${RESET}${YELLOW}\n"
            sleep 3
            exit 0
            ;;
        2)
            echo
            echo -e "${ORANGE}"
            warp-cli disconnect && wstatus
            echo -e "${RESET}${YELLOW}\n"
            sleep 3
            exit 0
            ;;
        3)
            echo
            echo -e "${ORANGE}"
            wstatus
            echo -e "${RESET}${YELLOW}\n"
            pressanykey
            ;;
        4)
            echo
            echo -e "${ORANGE}"
            warp-cli debug access-reauth
            echo -e "${RESET}${YELLOW}\n"
            pressanykey
            ;;
        5)
            echo
            echo -e "${ORANGE}"
            wenroll
            echo -e "${RESET}${YELLOW}\n"
            pressanykey
            ;;
	6)
            echo
            echo -e "${ORG}"
            warp-cli registration delete
            echo -e "${RESET}${YELLOW}\n"
            pressanykey
            ;;
        0|q|Q)
            echo
            echo -e "${GREEN}Thanks for using the script! ${RED}\m/${RESET}"
            echo
            sleep 1
            exit 0
            ;;
       *)
            echo
            echo -e "${RED}Wrong choice.. Please try again.${RESET}"
            sleep 1
	    pressanykey
      esac
done
}

menu
